﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pencil : MonoBehaviour {
    
    public TextAsset AssetText;

    private string Text { get { return AssetText.text; } }


    private string _oldText;

    public Book WriteBook;

    public LineRenderer Line;
    
    private Dictionary<char, LiteraController> _sortedPrefabls;

    private const string MovableLiters = "бвгджзклмнпрстфхцчшщ";
    
    void Update () {
        
        if (WriteBook == null)
        {
            Debug.LogError("Set write book!");
            return;
        }

        if (_sortedPrefabls == null)
        {
            _sortedPrefabls = new Dictionary<char, LiteraController>();

            var litersPrefabs = Resources.LoadAll<LiteraController>("Liters");

            foreach (var litera in litersPrefabs)
            {
                _sortedPrefabls.Add(litera.Litera, litera);
            }
        }

		if (_oldText != Text)
        {
            RebuildText();
            _oldText = Text;
        }
	}

    private void RebuildText()
    {
        if (Text == null || Text.Length < 1)
        {
            WriteBook.StartDraw();
            WriteBook.EndDraw();
            return;
        }

        WriteBook.StartDraw();

        //Debug.Log(char.IsLetter(' ') + " " + char.IsLetter('A'));

        //var words = Text.Split(Separators.ToCharArray());

        //char.is

        bool wordCount = false;
        int startWordIndex = -1;
        int count = 0;

        for (int i = 0; i < Text.Length; i++)
        {
            var liter = Text[i];
            
            if (!char.IsLetter(liter))
            {
                if (wordCount)
                {
                    DrawWord(startWordIndex, count);
                    wordCount = false;
                }

                if (liter == '@' || liter == '\n')
                {
                    if (liter == '@')
                        WriteBook.ReconfigureToCenterDraw();
                    else
                        WriteBook.NewLine(false);
                }

                LiteraController findedLitera;
                if (_sortedPrefabls.TryGetValue(liter, out findedLitera))
                    WriteBook.DrawLitera(findedLitera, Line);
            }
            else
            {
                if (!wordCount)
                {
                    startWordIndex = i;
                    count = 0;
                    wordCount = true;
                }

                count++;
            }
        }

        if (wordCount)
        {
            DrawWord(startWordIndex, count);
            wordCount = false;
        }

        WriteBook.EndDraw();
    }

    private void DrawWord(int startIndex, int count)
    {
        LiteraController findedLitera;

        var liters = new LiteraController[count];

        for (int i = 0; i < count; i++)
        {
            liters[i] = null;

            var liter = Text[i + startIndex];
            
            if (_sortedPrefabls.TryGetValue(liter, out findedLitera))
                liters[i] = findedLitera;
            //Sheet.DrawLitera(findedLitera, Line);
        }

        var ti = GetNearestTransitIndex(startIndex, count, WriteBook.CalculateCapacityLiters(liters)-1);

        if (ti == 0)
        {
            WriteBook.NewLine(WriteBook.CenterMode);
            ti = -1;
        }
        
        for (int i = 0; i < liters.Length; i++)
        {
            if (i == ti)
            {
                if (_sortedPrefabls.TryGetValue('-', out findedLitera))
                    WriteBook.DrawLitera(findedLitera, Line);
                WriteBook.NewLine(WriteBook.CenterMode);
            }

            if (liters[i] != null)
                WriteBook.DrawLitera(liters[i], Line);
        }
    }



    private int GetNearestTransitIndex(int startIndex, int count, int reqIndex)
    {
        if (reqIndex < -1)
            return -1;

        if (reqIndex <= 1)
            return 0;

        var movableLiters = MovableLiters.ToCharArray();

        for (int i = Mathf.Min(reqIndex, count-2); i > 1; i--)
        {
            if (System.Array.FindIndex(movableLiters, delegate (char inp) { return inp == Text[i+startIndex]; }) > -1)
            {
                return i;
            }
        }

        return 0;
    }
}

//аеёийоуыэюяъыь
//бвгджзклмнпрстфхцчшщ

//АаБбВвГгДдЕеЁёЖжЗзИиЙйКкЛлМмНнОоПпРрСсТтУуФфХхЦцЧчШшЩщЪъЫыЬьЭэЮюЯя
//АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдеёжзийклмнопрстуфхцчшщъыьэюя
//168

/*
 @Заголовок

Цель работы:

@Ознакомление с эмпирической

функцией распределения, гистограммой и полигоном, способами их построения и методикой их использования при статистических исследованиях экономических и производственных явлений
*/
