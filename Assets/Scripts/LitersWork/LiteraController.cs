﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(OldLitersDestroyer))]
public class LiteraController : MonoBehaviour {

    public char Litera;
    
    public Vector2 StartPosition = Vector2.one * -1;
    public Vector2 EndPosition = Vector2.one * -1;

    public float RightBorderOffset;

    public bool SetedStartPosition { get { return StartPosition.x >= 0 && StartPosition.y >= 0; } }
    public bool SetedEndPosition { get { return EndPosition.x >= 0 && EndPosition.y >= 0; } }

    private SpriteRenderer _rawRenderer;
    private SpriteRenderer _renderer { get {
            if (_rawRenderer == null)
                _rawRenderer = GetComponent<SpriteRenderer>();
            return _rawRenderer;
        } }
    
    public Vector2 CalculateStartPositionInWorld()
    {
        return SpriteToWorldPoint(StartPosition);
    }

    public Vector2 CalculateEndPositionInWorld()
    {
        return SpriteToWorldPoint(EndPosition);
    }

    private Vector2 SpriteToWorldPoint(Vector2 spritePoint)
    {
        return (Vector2)transform.position + ((spritePoint - _renderer.sprite.pivot) / _renderer.sprite.pixelsPerUnit);
    }

    public float GetWidth()
    {
        if (SetedEndPosition)
        {
            return (EndPosition.x - _renderer.sprite.pivot.x)/ _renderer.sprite.pixelsPerUnit + RightBorderOffset;
        }
        return ((_renderer.sprite.texture.width - _renderer.sprite.border.z) - _renderer.sprite.pivot.x) / _renderer.sprite.pixelsPerUnit;
    }

    public float CalculateRightBorderInWorld()
    {
        if (SetedEndPosition)
        {
            return CalculateEndPositionInWorld().x + RightBorderOffset;
        }
        
        return SpriteToWorldPoint(Vector2.one * (_renderer.sprite.texture.width-_renderer.sprite.border.z)).x;
    }

    public override string ToString()
    {
        return Litera.ToString();
    }
}
