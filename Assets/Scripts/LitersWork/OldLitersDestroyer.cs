﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class OldLitersDestroyer : MonoBehaviour {

    public int GenFrameNum = -1;
    public Sheet MyBuilder;

    private void Update()
    {
        if (GenFrameNum < 0)
        {
            return;
        }

        if (MyBuilder == null || GenFrameNum != MyBuilder.BuildedNum)
        {
            DestroyImmediate(gameObject);
        }
    }
}
