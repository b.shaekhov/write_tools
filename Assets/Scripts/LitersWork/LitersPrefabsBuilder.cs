﻿using UnityEngine;

[ExecuteInEditMode]
public class LitersPrefabsBuilder : MonoBehaviour {

    public bool Build;

    private void Update()
    {
        if (Build)
        {
            var litersPrefabs = Resources.LoadAll<Sprite>("LitersSource");

            var existPrefabs = Resources.LoadAll<LiteraController>("Liters");

            var parent = new GameObject("TESTSprites");

            foreach (var sprite in litersPrefabs)
            {
                string calcName = sprite.name.Remove(0, sprite.name.LastIndexOf("_") + 1);

                var existIndex = System.Array.FindIndex(existPrefabs, delegate (LiteraController curControl) { return curControl.Litera == calcName[0]; });

                if (existIndex > -1)
                    continue;

                var curSprite = new GameObject(calcName);
                curSprite.AddComponent<SpriteRenderer>().sprite = sprite;
                curSprite.AddComponent<LiteraController>().Litera = calcName[0];

                curSprite.transform.SetParent(parent.transform, true);
            }

            Build = false;
        }
    }
}
