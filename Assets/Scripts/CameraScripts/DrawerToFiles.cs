﻿using System;
using UnityEngine;

public class DrawerToFiles : MonoBehaviour {

    public int PixelsPerUnit = 35;
    Camera _cachedCam;
    Camera _camera { get { if (_cachedCam == null) _cachedCam = GetComponent<Camera>(); return _cachedCam;  } }

    RenderTexture _tmpTargetTexture;
    float _tmpOrtSize;
    Vector3 _tmpPos;
    Quaternion _tmpRot;

    public void SaveCameraState()
    {
        _tmpTargetTexture = _camera.targetTexture;
        _tmpOrtSize = _camera.orthographicSize;
        _tmpPos = _camera.transform.position;
        _tmpRot = _cachedCam.transform.rotation;
    }

    public void DrawSheetFile(Sheet sheet, bool reflect)
    {
        if (_camera.targetTexture != null && _camera.targetTexture != _tmpTargetTexture)
        {
            _camera.targetTexture = null;
            DestroyImmediate(_camera.targetTexture);
        }

        var targetTex = new RenderTexture((int)(sheet.SheetSize.x * PixelsPerUnit), (int)(sheet.SheetSize.y * PixelsPerUnit), 24);
        
        _camera.targetTexture = targetTex;
        _camera.orthographicSize = sheet.SheetSize.y / 2;
        _camera.transform.position = new Vector3(sheet.SheetSize.x / 2, sheet.SheetSize.y / 2, -1);

        _camera.transform.rotation = Quaternion.Euler(new Vector3(0, 0, reflect ? 180 : 0));

        _camera.Render();

        var newTex = ToTexture2D(targetTex);

        System.IO.File.WriteAllBytes(System.IO.Path.GetDirectoryName(Application.dataPath) + "/"+sheet.gameObject.name+".png", newTex.EncodeToPNG());

        DestroyImmediate(newTex);
        GC.Collect();
    }

    public void RestoreCameraState()
    {
        _camera.targetTexture = _tmpTargetTexture;
        _camera.orthographicSize = _tmpOrtSize;
        _camera.transform.position = _tmpPos;
        _cachedCam.transform.rotation = _tmpRot;

        _camera.Render();
    }

    Texture2D ToTexture2D(RenderTexture rTex)
    {
        Texture2D tex = new Texture2D(rTex.width, rTex.height, TextureFormat.RGB24, false);
        RenderTexture.active = rTex;
        tex.ReadPixels(new Rect(0, 0, rTex.width, rTex.height), 0, 0);
        tex.Apply();
        return tex;
    }
}
