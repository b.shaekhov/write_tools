﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RendererEvents : MonoBehaviour {

    public delegate void EmptyD();
    public event EmptyD OnPostRenderEvent;

    private void OnPostRender()
    {
        if (OnPostRenderEvent != null)
            OnPostRenderEvent();
    }
}
