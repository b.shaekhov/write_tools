﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMover : MonoBehaviour {

    public float Speed = 1;
    
	void Update () {
		if (Input.GetKey(KeyCode.F) && Input.GetKey(KeyCode.LeftShift))
        {
            transform.position += Vector3.forward * Time.deltaTime * Speed;
        }

        if (Input.GetKey(KeyCode.B) && Input.GetKey(KeyCode.LeftShift))
        {
            transform.position -= Vector3.forward * Time.deltaTime * Speed;
        }

        if (Input.GetKey(KeyCode.W) && Input.GetKey(KeyCode.LeftShift))
        {
            transform.position += Vector3.up * Time.deltaTime * Speed;
        }

        if (Input.GetKey(KeyCode.S) && Input.GetKey(KeyCode.LeftShift))
        {
            transform.position -= Vector3.up * Time.deltaTime * Speed;
        }

        if (Input.GetKey(KeyCode.D) && Input.GetKey(KeyCode.LeftShift))
        {
            transform.position += Vector3.right * Time.deltaTime * Speed;
        }

        if (Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.LeftShift))
        {
            transform.position -= Vector3.right * Time.deltaTime * Speed;
        }
    }
}
