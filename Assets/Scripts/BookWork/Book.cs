﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Book : MonoBehaviour
{

    public Vector2 SheetSize;
    public Vector2 SquareSize = Vector2.one * 5;

    public Sheet SheetPrefab;

    private List<Sheet> _mySheets = new List<Sheet>();

    public bool CenterMode { get { return _mySheets[_mySheets.Count - 1].CenterMode; } }

    public bool AutoDrawBook;

    private void Update()
    {
        /*if (AutoDrawBook)
        {
            AutoDrawBook = false;

            var drawer = Camera.main.GetComponent<DrawerToFiles>();
            
            if (drawer != null)
            {
                drawer.SaveCameraState();

                bool reflect = false;

                foreach (var s in _mySheets)
                {
                    drawer.DrawSheetFile(s, reflect);

                    reflect = !reflect;
                }

                drawer.RestoreCameraState();
            }
        }*/
    }

    public void StartDraw()
    {
        drawer = Camera.main.GetComponent<DrawerToFiles>();
        drawer.SaveCameraState();

        foreach (var s in _mySheets)
        {
            if (s != null)
                DestroyImmediate(s.gameObject);
        }

        _mySheets.Clear();

        AddSheet(false);

        GC.Collect();
    }

    int counter = 0;

    private void AddSheet(bool centerDraw)
    {
        if (_mySheets.Count > 0)
        {
            EndDrawCurSheet();
        }

        var newSheet = Instantiate(SheetPrefab.gameObject).GetComponent<Sheet>();
        newSheet.gameObject.name = SheetPrefab.gameObject.name + "_"+ counter;
        newSheet.transform.SetParent(transform, true);
        newSheet.SheetSize = SheetSize;
        newSheet.SquareSize = SquareSize;
        newSheet.StartDraw();

        if (centerDraw)
            newSheet.ReconfigureToCenterDraw();
        
        _mySheets.Add(newSheet);
        counter++;
    }

    public int CalculateCapacityLiters(params LiteraController[] liters)
    {
        return _mySheets[_mySheets.Count - 1].CalculateCapacityLiters(liters);
    }

    public void DrawLitera(LiteraController litera, LineRenderer connectionLine)
    {
        while (!_mySheets[_mySheets.Count - 1].DrawLitera(litera, connectionLine))
        {
            AddSheet(CenterMode);
        }
    }

    public void ReconfigureToCenterDraw()
    {
        while (!_mySheets[_mySheets.Count - 1].ReconfigureToCenterDraw())
        {
            AddSheet(CenterMode);
        }
    }

    /*public bool ReconfigureToLeftDraw()
    {
        return _mySheets[_mySheets.Count - 1].ReconfigureToLeftDraw();
    }*/

    public void NewLine(bool center)
    {
        if (!_mySheets[_mySheets.Count - 1].NewLine(center))
            AddSheet(CenterMode);
        /*while (!_mySheets[_mySheets.Count - 1].NewLine(center))
        {
            AddSheet(CenterMode);
        }*/
    }

    public void EndDraw()
    {
        EndDrawCurSheet();
        drawer.RestoreCameraState();
        GC.Collect();
    }

    bool flipper = false;
    DrawerToFiles drawer;

    private void EndDrawCurSheet()
    {
        _mySheets[_mySheets.Count - 1].EndDraw();

        if (AutoDrawBook)
        {
            drawer.DrawSheetFile(_mySheets[_mySheets.Count - 1], flipper);
            DestroyImmediate(_mySheets[_mySheets.Count - 1].gameObject);
            _mySheets.RemoveAt(_mySheets.Count - 1);
            GC.Collect();

            flipper = !flipper;
        } else
        {
            _mySheets[_mySheets.Count - 1].gameObject.SetActive(false);
        }
    }
}
