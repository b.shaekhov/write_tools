﻿using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof(Sheet))]
public class SheetLinesDrawer : MonoBehaviour {

    public Sheet BaseSheet;
    public bool VerticalLines = true;
    public bool HorizontalLines = true;
    private Material mat;

    private void Reset()
    {
        BaseSheet = GetComponent<Sheet>();
    }

    private void Start()
    {
        //Camera.main.GetComponent<RendererEvents>().OnPostRenderEvent += PostRender;
    }

    private void OnDrawGizmos()
    {
        PostRender();
    }

    public void PostRender()
    {
        if (mat == null)
            mat = new Material(Shader.Find("UI/Default"));

        Vector2 offset = BaseSheet.SquareOffset;

        GL.PushMatrix();
        mat.SetPass(0);

        GL.Begin(GL.LINES);

        GL.Color(Color.red);

        DrawLine(Vector3.zero, Vector3.up * BaseSheet.SheetSize.y);
        DrawLine(Vector3.zero, Vector3.right * BaseSheet.SheetSize.x);

        DrawLine(Vector3.up * BaseSheet.SheetSize.y, Vector3.up * BaseSheet.SheetSize.y + Vector3.right * BaseSheet.SheetSize.x);
        DrawLine(Vector3.right * BaseSheet.SheetSize.x, Vector3.up * BaseSheet.SheetSize.y + Vector3.right * BaseSheet.SheetSize.x);

        GL.Color(Color.gray);

        if (VerticalLines)
        {
            while (offset.x <= BaseSheet.SheetSize.x)
            {
                DrawLine(Vector3.right * offset.x, Vector3.up * BaseSheet.SheetSize.y + Vector3.right * offset.x);

                offset.x += BaseSheet.SquareSize.x;
            }
        }

        if (HorizontalLines)
        {
            while (offset.y <= BaseSheet.SheetSize.y)
            {
                DrawLine(Vector3.up * offset.y, Vector3.up * offset.y + Vector3.right * BaseSheet.SheetSize.x);

                offset.y += BaseSheet.SquareSize.y;
            }
        }

        GL.End();
        GL.PopMatrix();
    }

    void DrawLine(Vector3 start, Vector3 end)
    {
        GL.Vertex(start);
        GL.Vertex(end);
    }

    private void OnDestroy()
    {
        var cam = Camera.main;
        if (cam == null) return;

        var re = cam.GetComponent<RendererEvents>();
        if (re == null) return;

        re.OnPostRenderEvent -= PostRender;
    }
}
