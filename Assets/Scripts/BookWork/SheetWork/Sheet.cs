﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sheet : MonoBehaviour {

    public Vector2 SheetSize;
    public Vector2 SquareOffset;

    private Vector2 StartWriteLeftBorder { get { return new Vector2(SquareSize.x/2 + SquareOffset.x, SheetSize.y - (SheetSize.y + SquareOffset.y) % SquareSize.y + SquareSize.y / 3 - SquareSize.y); } }
    private Vector2 StartWriteCenter { get { return new Vector2(SheetSize.x / 2, StartWriteLeftBorder.y); } }

    public Vector2 SquareSize = Vector2.one*5;


    private int _buildedNum = -1;
    public int BuildedNum { get { return _buildedNum; } }

    private bool _centerMode;
    public bool CenterMode { get { return _centerMode; } }
    private bool _newLine;
    private LiteraController _lastLitera;
    private Transform _centerParent;

    private Vector2 _cursorPosition;
    private bool _drawStarted;

    public void StartDraw()
    {
        _cursorPosition = StartWriteLeftBorder;

        _buildedNum++;
        _drawStarted = true;

        NewLine(false);
    }
    
    public int CalculateCapacityLiters(params LiteraController[] liters)
    {
        float sumWidth = 0;

        for (int i = 0; i < liters.Length; i++)
        {
            if (liters[i] != null)
                sumWidth += liters[i].GetWidth();

            if (_centerMode)
            {
                if (_centerParent.position.x - sumWidth / 2 <= 0)
                    return i;
            } else
            {
                if (_cursorPosition.x + sumWidth >= SheetSize.x)
                    return i;
            }
        }

        return -1;
    }

    /*public bool WriteToCenter(LiteraController litera, LineRenderer connectionLine)
    {
        if (!_centerMode)
        {
            if (!NewLine(true))
            {
                return false;
            }
        }
        return DrawLitera(litera, connectionLine);
    }

    public bool WriteToLeftBorder(LiteraController litera, LineRenderer connectionLine)
    {
        if (_centerMode)
        {
            if (!NewLine(false))
            {
                return false;
            }
        }
        return DrawLitera(litera, connectionLine);
    }*/

    public bool DrawLitera(LiteraController litera, LineRenderer connectionLine)
    {
        if (!_drawStarted)
        {
            Debug.LogError("Draw not initialised!");
            return false;
        }

        if (litera == null)
        {
            Debug.LogError("litera not found!");
            return false;
        }

        float literaWidth = litera.GetWidth();

        string name = litera.name;
        litera = Instantiate(litera.gameObject, _cursorPosition, litera.gameObject.transform.rotation, transform).GetComponent<LiteraController>();
        var destroyer = litera.GetComponent<OldLitersDestroyer>();
        destroyer.MyBuilder = this;
        destroyer.GenFrameNum = BuildedNum;

        litera.name = name;

        if (_centerMode)
        {
            if (_centerParent.position.x - literaWidth/2 < 0)
            {
                if (!NewLine(_centerMode))
                {
                    DestroyImmediate(litera.gameObject);
                    return false;
                }
                litera.transform.position = _cursorPosition;
            }

            litera.transform.SetParent(_centerParent, true);

            _cursorPosition.x += literaWidth / 2;
            _centerParent.position -= Vector3.right * literaWidth / 2;
        }
        else
        {
            if (_cursorPosition.x + literaWidth >= SheetSize.x)
            {
                if (!NewLine(_centerMode))
                {
                    DestroyImmediate(litera.gameObject);
                    return false;
                }
                litera.transform.position = _cursorPosition;
            }
            
            _cursorPosition.x += literaWidth;
        }

        if (_lastLitera != null && _lastLitera.SetedEndPosition && litera.SetedStartPosition)
        {
            var lineRend = Instantiate(connectionLine.gameObject, _lastLitera.CalculateEndPositionInWorld(), connectionLine.transform.rotation).GetComponent<LineRenderer>();
            destroyer = lineRend.GetComponent<OldLitersDestroyer>();

            destroyer.MyBuilder = this;
            destroyer.GenFrameNum = BuildedNum;

            if (_centerMode)
                lineRend.transform.SetParent(_centerParent, true);
            else
                lineRend.transform.SetParent(transform, true);

            lineRend.name += _lastLitera.Litera + "_" + litera.Litera;
            lineRend.SetPosition(0, Vector3.zero);
            lineRend.SetPosition(1, lineRend.transform.InverseTransformPoint(litera.CalculateStartPositionInWorld()));
        }

        _lastLitera = litera;

        _newLine = false;

        return true;
    }

    public bool ReconfigureToCenterDraw()
    {
        if (_centerMode)
            return true;

        if (_newLine)
        {
            _lastLitera = null;
            _centerMode = true;

            _cursorPosition.x = StartWriteCenter.x;
            NewCenterParent();

            return true;
        }

        return NewLine(true);
    }

    /*public bool ReconfigureToLeftDraw()
    {
        if (!_centerMode)
            return true;

        if (_newLine)
        {
            if (_centerParent != null)
                DestroyImmediate(_centerParent.gameObject);

            _lastLitera = null;
            _centerMode = false;

            _cursorPosition.x = StartWriteLeftBorder.x;

            return true;
        }

        return NewLine(false);
    }*/

    public bool NewLine(bool center)
    {
        _lastLitera = null;
        _centerMode = center;

        _newLine = true;

        _cursorPosition.y -= SquareSize.y;

        if (_centerMode)
        {
            _cursorPosition.x = StartWriteCenter.x;
            NewCenterParent();
        }
        else
            _cursorPosition.x = StartWriteLeftBorder.x;

        return _cursorPosition.y > SquareSize.y;
    }

    private void NewCenterParent()
    {
        _centerParent = (new GameObject("CenterParent")).transform;

        var destr = _centerParent.gameObject.AddComponent<OldLitersDestroyer>();
        destr.MyBuilder = this;
        destr.GenFrameNum = BuildedNum;

        _centerParent.SetParent(transform, true);
        _centerParent.position = _cursorPosition;
    }

    public void EndDraw()
    {
        _drawStarted = false;
    }
}
