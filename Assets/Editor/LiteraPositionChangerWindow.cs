﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class LiteraPositionChangerWindow : EditorWindow {

    string _windowHeader;
    SerializedProperty _serProp;
    Texture2D _workTexture;
    Texture2D _background;
    Texture2D _point;
    Vector2 _scloolViewPos;
    Vector2 _result;
    bool _writeResult;

    public void InitWindow(string header, SerializedProperty property)
    {
        _result = property.vector2Value;

        _windowHeader = header;
        _serProp = property;
        var renderer = ((LiteraController)_serProp.serializedObject.targetObject).GetComponent<SpriteRenderer>();

        if (renderer != null)
            _workTexture = renderer.sprite.texture;

        _background = new Texture2D(1, 1);
        _background.SetPixel(0, 0, Color.green);
        _background.Apply();

        _point = new Texture2D(1,1);
        _point.SetPixel(0, 0, Color.red);
        _point.Apply();
    }

    void OnGUI()
    {
        _scloolViewPos = GUILayout.BeginScrollView(_scloolViewPos);

        GUILayout.Label(_windowHeader, EditorStyles.boldLabel);
        
        if (_workTexture == null)
        {
            GUILayout.Label("Установите спрайт!");
            return;
        }

        var lastRect = GUILayoutUtility.GetLastRect();

        var texturePos = new Rect(0, lastRect.y + lastRect.height, _workTexture.width, _workTexture.height);

        GUILayout.Label("", GUILayout.Width(_workTexture.width), GUILayout.Height(_workTexture.height));
        
        GUI.DrawTexture(texturePos, _background);
        GUI.DrawTexture(texturePos, _workTexture);
        
        if (Event.current.type == EventType.MouseDown)
        {
            if (Event.current.mousePosition.x >= 0 && Event.current.mousePosition.x < texturePos.width &&
                Event.current.mousePosition.y >= texturePos.y && Event.current.mousePosition.y <= texturePos.y+ texturePos.height)
            _writeResult = true;
        }
        
        if (_writeResult) {
            _result.x = Event.current.mousePosition.x;
            //Debug.Log(Event.current.mousePosition.y);
            _result.y = texturePos.height - (Event.current.mousePosition.y - texturePos.y);//texturePos.height - (Event.current.mousePosition.y - texturePos.y);
        }
        
        GUI.DrawTexture(new Rect(_result.x-5, texturePos.height-_result.y+ texturePos.y - 5, 10, 10), _point);

        if (GUILayout.Button("End"))
        {
            _serProp.vector2Value = _result;
            _serProp.serializedObject.ApplyModifiedProperties();
            Close();
        }

        GUILayout.EndScrollView();

        if (_writeResult)
            Repaint();

        if (Event.current.type == EventType.MouseUp)
        {
            _writeResult = false;
        }
    }
}
