﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(LiteraController))]
public class LiteraControllerEditor : Editor
{
    SerializedProperty _startPosProperty;
    SerializedProperty _endPosProperty;

    void OnEnable()
    {
        _startPosProperty = serializedObject.FindProperty("StartPosition");
        _endPosProperty = serializedObject.FindProperty("EndPosition");
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        
        if (GUILayout.Button("EditStartPosition"))
        {
            LiteraPositionChangerWindow window = (LiteraPositionChangerWindow)EditorWindow.GetWindow(typeof(LiteraPositionChangerWindow));
            window.InitWindow("EditStartPosition", _startPosProperty);
            window.Show();
        }

        if (GUILayout.Button("EditEndPosition"))
        {
            LiteraPositionChangerWindow window = (LiteraPositionChangerWindow)EditorWindow.GetWindow(typeof(LiteraPositionChangerWindow));
            window.InitWindow("EditEndPosition", _endPosProperty);
            window.Show();
        }

        //serializedObject.Update();
        //EditorGUILayout.PropertyField(_startPosProperty);
        //EditorGUILayout.PropertyField(_endPosProperty);
        //serializedObject.ApplyModifiedProperties();
    }
}
